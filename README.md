# Account Case Study
Modeling account case study with VMJ and WinVMJ

## Features:
- BankAccount (Core)
- Overdraft
- Interest
- InterestEstimation
- CreditWorthiness
- DailyLimit

## Product:
- BasicAccount (Core only)
- DailyLimitAccount (Core, DailyLimit)
- OverdraftAccount (Core, Overdraft)


